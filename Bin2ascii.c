#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iso646.h>

#include "Bin2ascii.h"

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Display an arbitrary buffer as ascii using various means
/// HIFN	WARNING: most if not all of those conversions are ambiguous
/// HIFN	WARNING: NOT MULTITHREAD SAFE
/// HIPAR	Buffer/Any bytes - Pass NULL to free the internal buffer and return
/// HIPAR	Size/Size of the buffer to convert
/// HIPAR	Hex/display non-ascii as \hh (hexa), '.' otherwise
/// HIPAR	Name/Use NULL to US and DEL for 0~31-127 representation (overrides previous)
/// HIPAR	Ctrl/Use ^@ to ^_ for 0~31 representation (overrides previous)
/// HIPAR	Escape/Use \a, \b, \t, \n, \v, \f, \r for 7~13 representations (overrides previous)
/// HIRET	Converted string, auto-allocated (don't free it in case of multiple calls)
///////////////////////////////////////////////////////////////////////////////
char *Bin2ascii(char *Buffer, int Size, BOOL Hex, BOOL Name, BOOL Ctrl, BOOL Escape) {
	char *NameLookup[256]={	// char 0~31 and 127 only
		"<NUL>", "<SOH>", "<STX>", "<ETX>", "<EOT>", "<ENQ>", "<ACK>", "<BEL>", 
		"<BS>",  "<TAB>", "<LF>",  "<VT>",  "<FF>",  "<CR>",  "<SO>",  "<SI>", 
		"<DLE>", "<DC1>", "<DC2>", "<DC3>", "<DC4>", "<NAK>", "<SYN>", "<ETB>", 
		"<CAN>", "<EM>",  "<SUB>", "<ESC>", "<FS>",  "<GS>",  "<RS>",  "<US>" };
	NameLookup[127]="<DEL>";
	char *CtrlLookup[256]={	// char 0~31 only
							"^@", "^A", "^B", "^C", "^D", "^E", "^F", "^G", 
							"^H", "^I", "^J", "^K", "^L", "^M", "^N", "^O",
							"^P", "^Q", "^R", "^S", "^T", "^U", "^V", "^W", 
							"^X", "^Y", "^Z", "^[", "^\\", "^]", "^^", "^_"};
	char *EscLookup[256]={0, 0, 0, 0, 0, 0, 0, "\\a", // char 7~13 only
						  "\\b", "\\t", "\\n", "\\v", "\\f", "\\r"};
	static char* Dest=NULL;
	static int CurrentSize=0;
	char *P;
	unsigned char c;
	int i;
	
	// In case you want to clean things up for Valgrind for instance
	if (Buffer==NULL) { free(Dest); Dest=NULL; CurrentSize=0; return ""; }
	
	if (Size==0) return "";
	
	if (CurrentSize<5*Size) Dest=realloc(Dest, CurrentSize=5*Size+16);
	if (Dest==NULL) return "ERROR: OUT OF MEMORY";
	P=Dest;
	
	for (i=0; i<Size; i++) {
		c=((unsigned char *)Buffer)[i];
		if (isprint(c))					{ *P++=Buffer[i];					continue; }
		if (Escape and c>=7 and c<=13)	{ strcpy(P, EscLookup[c]);  P+=2;	continue; }
		if (Ctrl and c<=31)				{ strcpy(P, CtrlLookup[c]); P+=2;	continue; }
		if (Name and (c<=31 or c==127))	{ strcpy(P, NameLookup[c]); P+=strlen(NameLookup[c]); continue; }
		if (Hex)						{ sprintf(P, "\\%02x", c);  P+=3;	continue; }
		*P++='.';
	}
	*P='\0';
	return Dest;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Simpler version of the above: converts all bytes to XX hex pairs
/// HIFN	WARNING: NOT MULTITHREAD SAFE
/// HIPAR	Separator / Separates them with optional character. 0 if not wanted
///////////////////////////////////////////////////////////////////////////////
char *Bin2hex(char *Buffer, int Size, char Separator) {
	static char* Dest=NULL;
	static int CurrentSize=0;
	char *P;
	unsigned char c;
	int i;
	
	// In case you want to clean things up for Valgrind for instance
	if (Buffer==NULL) { free(Dest); Dest=NULL; CurrentSize=0; return ""; }
	
	if (Size==0) return "";
	
	if (CurrentSize<3*Size) Dest=realloc(Dest, CurrentSize=3*Size+16);
	if (Dest==NULL) return "ERROR: OUT OF MEMORY";
	P=Dest;
	
	for (i=0; i<Size; i++) {
		c=((unsigned char *)Buffer)[i];
		if (Separator!=0 and i!=Size-1) { sprintf(P, "%02X%c", c, Separator);  P+=3; }
		else { sprintf(P, "%02X", c);  P+=2; }
	}
	*P='\0';
	return Dest;
}

#ifndef isascii
	#define isascii(c) (((c) & ~0x7f) == 0)
#endif
#ifndef isblank
	#define isblank(c) ((c)==' ' or (c)=='\t')
#endif

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Give the properties of a character
/// HIFN	WARNING: NOT MULTITHREAD SAFE
/// HIPAR	c/Normally of type char
/// HIRET	Informative string
///////////////////////////////////////////////////////////////////////////////
char *CharType(int c) {
	static char Dest[100]="";
	//int L;
//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wsequence-point"
	sprintf(Dest, "'%c' char 0x%02x is%s%s%s%s%s%s%s%s%s%s%s%s%s",
		isprint(c) ? c : '.', 
		c,
		isalnum(c) ? " alnum" : "",
		isalpha(c) ? " alpha" : "",
		isascii(c) ? " ascii" : " N/A",	// If not ascii, then none of the other criteria are met
		isblank(c) ? " blank" : "",
		iscntrl(c) ? " cntrl" : "",
		isdigit(c) ? " digit" : "",
		isxdigit(c)? " xdigit": "",
		islower(c) ? " lower" : "",
		isupper(c) ? " upper" : "",
		isgraph(c) ? " graph" : "",
		isprint(c) ? " print" : "",
		ispunct(c) ? " punct" : "",
		isspace(c) ? " space" : "");
//#pragma GCC diagnostic pop
//	sprintf(Dest+L, "%s", Q ? "" : " N/A");
	return Dest;
}

#ifdef TEST_B2A	// Testing and debugging, compile with -DTEST_B2A
int main(int argc, char *argv[]) {
	int i, a, b, c, d;
//	unsigned char Str[]={'a', 'b', 'c', 0, '\n', 127, 200, '\r', '\\', ' ', '0', 233 };
	unsigned char Str[256]={0}; 
	for (i=0; i<256; i++) Str[i]=(unsigned char)i;
	
	// Try all 8 possible option combinations
	for (a=0; a<2; a++)
		for (b=0; b<2; b++)
			for (c=0; c<2; c++)
				for (d=0; d<2; d++)
					printf("H%d N%d C%d E%d: %s\n\n", a ,b ,c ,d,
						   Bin2ascii((char*)Str, sizeof(Str), a, b, c, d));

	// Details on each possible char
	for (i=0; i<sizeof(Str); i++) 
		printf("%s\n", CharType(Str[i]));
	
	return 0;
}
#endif
