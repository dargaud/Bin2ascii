TARGET=Bin2ascii
GCC=gcc
COPT=-Wall

all:
	$(GCC) $(COPT) -c $(TARGET).c
	$(GCC) $(COPT) -o  Test_$(TARGET) $(TARGET).c -DTEST_B2A
